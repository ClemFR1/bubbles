# Bubbles

![bubbles.png](assets/bubbles.png)

* [Code of Conduct](#code-of-conduct)
* [Pipeline](#pipeline)
* [Technologies](#technologies)
* [User Documentation for the Bubbles App](#user-documentation-for-the-bubbles-app)
* [Introduction](#introduction)
* [Launch](#launch)
* [Functionalities](#functionalities)
  * [Select a member and make some action on it](#select-a-member-and-make-some-action-on-it)
  * [Add one member](#add-one-member)
  * [Add one relation](#add-one-relation)
  * [Undo an action](#undo-an-action)
  * [Tree Registration](#tree-registration)

## Code of Conduct

When a user wants to add a feature, the flow is as follows:

1. The user creates a new branch from the `main` branch, prefixed by `develop/` and the name of the feature.
2. The user makes the changes in the new branch.
3. The user creates a pull request from the new branch to the `main` branch.
4. The user waits for the pull request to be reviewed and approved by at least one other user.

## Pipeline

The pipeline contains 2 steps (`.gitlab-ci.yml`) :

1. `build` : This step builds the application, creates artifacts (only on the `main` branch).
2. `test` : This step runs the tests of the application.

⚠️: it's highly recommended to run the tests locally before pushing the code.

⚠️: it's highly recommended to run the pipeline before merging the pull request.

## Technologies

| Tech.  | Version |
|--------|---------|
| JDK    |         |
| JavaFx |         |
| JUnit  |         |
| Jacoco |         |


## User Documentation for the Bubbles App

## Introduction

Welcome to the family tree management app. This app allows you to easily create 
and manage your family tree. You can add members, define parental and marital relationships, 
cancel actions, register your tree, and delete or modify existing members.

## Launch
To start the application you must run the class Main.java, select a json file 
available to test/ resources or one of your creation. Then you can interact with the tree.

## Functionalities

![buttonBar.png](assets%2FbuttonBar.png)

### Member manipulation
1. **Manipulation** : The manipulation of the limbs is done by pressing the manipulation button (2). 
When this mode is active it is necessary to keep clicking on a member by moving the mouse cursor to move the member.

### Select a member and make some action on it
1. **Get selection mode** : Press on the selection button (1) on the bottom app bar.
2. **Clic on a family member** : Member is now highlighted and all relations associated. 
This action also activate the "Action for" section of the app bar who include delete and update actions for member.
3. **Update a member** : When a member is selected, user can update name and avatar of this member.
   - To do that action, user have to press the "Update" button (button 6). An update pop-up is showed whit two fields.
   - Set at least one field and press validate.
   - The member is now updated.
4. **Delete a member** : Selected member and all these relations is deleted.
   - Press "Delete".
5. **Relationship research** : Looks for the shortest relationship between two members.
   - Press "What's relation"
   - Enter a name of another member
   - If the two members are bound by relations, the relations are restored 
   otherwise it is indicated that no relationship exists between these two members.

### Add one member
1. **Member addition**: Press "Add member" button (4).
2. **Create the member** : To create a member simply click on the tree drawing window. A pop-up then appears, 
it contains two fields the first is the name of the member and the second the url corresponding to his avatar (avatar displayed in the list of relations).
3. **Consequences** : The new member is displayed on the main tree interface, 
if a member already exists with this name, it is renamed by suffixing the name with a number .

### Add one relation
1. **Relationship addition**: Press "Add relations" button (3).
2. **Create the link** : To create a link, click on a member and hold the click to move to another member before releasing it.
3. **Choose relation type** : If the relationship is possible a pop-up appears, the user can then choose the type of relationship (parental or marital).
4. **Consequences** : The relationship is displayed on the main tree interface and is added to the list of relationships.

### Undo an action
1. **Undo an action** : Press the `ctrl + Z` keys simultaneously, and the tree will return to its previous state.

### Tree Registration
1. **Save by shortcut** : Press the `ctrl + S` keys simultaneously, and the tree will be safe on a JSON file.
2. **Save when closing the application** : When closing the application if a registration is required, this is done without asking the user to do so.
