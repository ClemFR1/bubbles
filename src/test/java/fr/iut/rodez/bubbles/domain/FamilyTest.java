package fr.iut.rodez.bubbles.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FamilyTest {

    Family family;

    FamilyMember paternalGrandfather;
    FamilyMember paternalGrandmother;
    FamilyMember maternalGrandfather;
    FamilyMember maternalGrandmother;
    FamilyMember father;
    FamilyMember mother;
    FamilyMember son;
    FamilyMember daughter;


    /**
     * The family looks like :
     *      pGf --- pGm      mGf --- mGm
     *           |                |
     *           |                |
     *        father --------- mother
     *                   |
     *                -------
     *               |       |
     *              son  daughter
     */
    @BeforeEach
    void setUp() {
        //Given a family

        paternalGrandfather = new FamilyMember(new Identity(UUID.randomUUID(), "pGf", null), new Position(0, 0));
        paternalGrandmother = new FamilyMember(new Identity(UUID.randomUUID(), "pGm", null), new Position(10, 10));

        maternalGrandfather = new FamilyMember(new Identity(UUID.randomUUID(), "mGf", null),new Position(20, 20));
        maternalGrandmother = new FamilyMember(new Identity(UUID.randomUUID(), "mGm", null), new Position(30, 30));

        father = new FamilyMember(new Identity(UUID.randomUUID(), "dad", null),new Position(40, 40));
        mother = new FamilyMember(new Identity(UUID.randomUUID(), "mom", null), new Position(50, 50));

        son = new FamilyMember(new Identity(UUID.randomUUID(), "son", null), new Position(60, 60));
        daughter = new FamilyMember(new Identity(UUID.randomUUID(), "daughter", null), new Position(70, 70));

        paternalGrandfather.parentOf(father);
        paternalGrandmother.parentOf(father);

        maternalGrandfather.parentOf(mother);
        maternalGrandmother.parentOf(mother);

        father.parentOf(son);
        father.parentOf(daughter);

        mother.parentOf(son);
        mother.parentOf(daughter);

        Set<FamilyMember> familyMembers = new HashSet<>(
                Arrays.asList(paternalGrandfather, paternalGrandmother,
                        maternalGrandfather, maternalGrandmother,
                        mother, father, son, daughter));

        family = new Family("name", familyMembers);
    }

    @Test
    void deleteOneMemberOfTheFamily() {
        assertEquals(8, family.members().size());

        //When removing the father of the family graph
        family.deleteMember(father);

        //Then
        assertEquals(7, family.members().size());
    }

    @Test
    void addOneMemberToTheFamily() {
        //Given a new member
        FamilyMember aNewBaby = new FamilyMember(new Identity(UUID.randomUUID(), "baby", null), new Position(80, 80));
        assertEquals(8, family.members().size());

        //When
        family.addingMember(aNewBaby);

        //Then
        assertEquals(9, family.members().size());
        assertTrue(family.members().contains(aNewBaby));
    }
}
