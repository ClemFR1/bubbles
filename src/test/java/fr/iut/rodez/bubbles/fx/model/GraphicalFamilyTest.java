package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class GraphicalFamilyTest {

    GraphicalFamily graphicalFamily;
    Family family;

    FamilyMember paternalGrandfather;
    FamilyMember paternalGrandmother;
    FamilyMember maternalGrandfather;
    FamilyMember maternalGrandmother;
    FamilyMember father;
    FamilyMember mother;
    FamilyMember son;
    FamilyMember daughter;


    /**
     * The family looks like :
     *      pGf --- pGm      mGf --- mGm
     *           |                |
     *           |                |
     *        father --------- mother
     *                   |
     *                -------
     *               |       |
     *              son  daughter
     */
    @BeforeEach
    void setUp() {
        //Given a family

        paternalGrandfather = new FamilyMember(new Identity(UUID.randomUUID(), "pGf", null), new Position(0, 0));
        paternalGrandmother = new FamilyMember(new Identity(UUID.randomUUID(), "pGm", null), new Position(10, 10));

        maternalGrandfather = new FamilyMember(new Identity(UUID.randomUUID(), "mGf", null),new Position(20, 20));
        maternalGrandmother = new FamilyMember(new Identity(UUID.randomUUID(), "mGm", null), new Position(30, 30));

        father = new FamilyMember(new Identity(UUID.randomUUID(), "dad", null),new Position(40, 40));
        mother = new FamilyMember(new Identity(UUID.randomUUID(), "mom", null), new Position(50, 50));

        son = new FamilyMember(new Identity(UUID.randomUUID(), "son", null), new Position(60, 60));
        daughter = new FamilyMember(new Identity(UUID.randomUUID(), "daughter", null), new Position(70, 70));

        paternalGrandfather.parentOf(father);
        paternalGrandmother.parentOf(father);

        maternalGrandfather.parentOf(mother);
        maternalGrandmother.parentOf(mother);

        father.parentOf(son);
        father.parentOf(daughter);

        mother.parentOf(son);
        mother.parentOf(daughter);

        Set<FamilyMember> familyMembers = new HashSet<>(
                Arrays.asList(paternalGrandfather, paternalGrandmother,
                        maternalGrandfather, maternalGrandmother,
                        mother, father, son, daughter));

        family = new Family("familyName", familyMembers);

        graphicalFamily = new GraphicalFamily(family);
    }

    @Test
    void shouldBeAbleToCreateGraphicalFamily() {
        //Given a family

        //When
        GraphicalFamily graphicalFamily = new GraphicalFamily(family);

        //Then
        assertEquals(8, graphicalFamily.members.size());
        assertEquals(8, graphicalFamily.relations.size());
    }

    @Test
    void deleteOneMemberOfTheGraphicalFamily() {
        //Given a graphical member corresponding to the father
        GraphicalFamilyMember graphicalFather = graphicalFamily.members
                .stream()
                .filter(member -> member.getFamilyMember().getIdentity().getName().equals(father.identity.getName()))
                .findFirst().orElse(null);

        //TODO change initial number of relation when in love relation are implement
        assertEquals(4, father.relations().size());
        assertEquals(8, family.members().size());
        assertEquals(8, graphicalFamily.members.size());
        assertEquals(8, graphicalFamily.relations.size());

        //When removing the father of the family graph
        graphicalFamily.deleteOneMember(graphicalFather);

        //Then
        assertEquals(0, father.relations().size());
        assertEquals(7, family.members().size());
        assertEquals(7, graphicalFamily.members.size());
        assertEquals(4, graphicalFamily.relations.size());
    }

    @Test
    void addOneMemberToTheGraphicalFamily() {
        //Given a graphical member corresponding to the last born
        GraphicalFamilyMember aNewBaby = new GraphicalFamilyMember(
                new FamilyMember(
                        new Identity(UUID.randomUUID(), "baby", null),
                        new Position(80, 80)));

        assertEquals(8, family.members().size());

        //When
        graphicalFamily.addingOneMember(aNewBaby);

        //Then
        assertEquals(9, family.members().size());
    }

    @Test
    void shouldReturnTrueWhenNameIsAlreadyUse() {
        //Then
        assertTrue(graphicalFamily.nameAlreadyExist("dad"));
    }

    @Test
    void shouldReturnFalseWhenNameIsUnused() {
        //Then
        assertFalse(graphicalFamily.nameAlreadyExist("unused name"));
    }

    @Test
    void shouldReturnThreeWhenThreeMemberHaveSameName() {
        //Given
        Identity identity1 = new Identity(UUID.randomUUID(),"antoine",null);
        Identity identity2 = new Identity(UUID.randomUUID(),"antoine",null);
        Identity identity3 = new Identity(UUID.randomUUID(),"antoine",null);
        FamilyMember familyMember1 = new FamilyMember(identity1,new Position(0,0));
        FamilyMember familyMember2 = new FamilyMember(identity2,new Position(0,0));
        FamilyMember familyMember3 = new FamilyMember(identity3,new Position(0,0));

        //When
        graphicalFamily.members.add(new GraphicalFamilyMember(familyMember1));
        graphicalFamily.members.add(new GraphicalFamilyMember(familyMember2));
        graphicalFamily.members.add(new GraphicalFamilyMember(familyMember3));

        //Then
        assertEquals(3L,graphicalFamily.verifyExistingName("antoine"));
    }
}
