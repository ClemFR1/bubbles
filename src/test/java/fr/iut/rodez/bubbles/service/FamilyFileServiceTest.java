package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import fr.iut.rodez.bubbles.domain.*;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyFileServiceTest {

    static final String FAMILY_SAMPLE = "{\"name\":\"Doe\",\"members\":[{\"id\":\"ee8d4015-4ab0-4b7d-89a6-db1b902b472d\",\"name\":\"Jeannot\",\"picture\":\"https://cdn-icons-png.flaticon.com/512/3541/3541871.png\",\"position\":{\"x\":50,\"y\":50}},{\"id\":\"e5b9c1aa-1355-4fa6-8b87-a4cc0366ab85\",\"name\":\"Lucette\",\"picture\":\"https://cdn-icons-png.flaticon.com/512/3541/3541871.png\",\"position\":{\"x\":150,\"y\":150}},{\"id\":\"8d1299af-d1f1-48c6-ae6e-e48efbdb33bd\",\"name\":\"Michel\",\"picture\":\"https://cdn-icons-png.flaticon.com/512/3541/3541871.png\",\"position\":{\"x\":150,\"y\":50}}],\"relations\":[{\"from\":\"ee8d4015-4ab0-4b7d-89a6-db1b902b472d\",\"to\":\"e5b9c1aa-1355-4fa6-8b87-a4cc0366ab85\",\"type\":\"PARENT\"},{\"from\":\"e5b9c1aa-1355-4fa6-8b87-a4cc0366ab85\",\"to\":\"8d1299af-d1f1-48c6-ae6e-e48efbdb33bd\",\"type\":\"PARENT\"},{\"from\":\"8d1299af-d1f1-48c6-ae6e-e48efbdb33bd\",\"to\":\"ee8d4015-4ab0-4b7d-89a6-db1b902b472d\",\"type\":\"PARENT\"}]}";

    @Test
    void shouldReturnLoadedWhenFileIsCorrectlyLoaded() {
        // Given
        var file = new File("src/test/resources/family.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.Loaded loaded = assertInstanceOf(FamilyLoadingResult.Loaded.class, result);

        Family family = loaded.family();
        assertEquals("Doe", family.name());

        Set<FamilyMember> members = family.members();
        assertEquals(9, members.size());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsIncorrect() {
        // Given
        var file = new File("src/test/resources/incorrect-path.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(FileNotFoundException.class, loadingError.cause());
        FileNotFoundException exception = assertInstanceOf(FileNotFoundException.class, loadingError.cause());
        //assertEquals("src/test/resources/incorrect-path.json (No such file or directory)", exception.getMessage());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotJson() {
        // Given
        var file = new File("src/test/resources/not-json.txt");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotValidJson() {
        // Given
        var file = new File("src/test/resources/invalid.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }

    @Test
    void shouldReturnNullWhenCreatingFamilyWithInvalidJson() {
        assertNull(FamilyService.fromJson("invalid"));
    }

    @Test
    void shouldLoadFamilyFromAValidJson() {
        Family loaded = FamilyService.fromJson(FAMILY_SAMPLE);

        assertEquals(loaded.name(), "Doe");
        assertEquals(loaded.members().size(), 3);

        List<String> members = new ArrayList<>();
        members.add("Jeannot");
        members.add("Lucette");
        members.add("Michel");

        for (FamilyMember m : loaded.members()) {
            if (members.contains(m.getIdentity().getName())) {
                members.remove(m.getIdentity().getName());
            } else {
                fail("Family contains stranger member (" + m.getIdentity().getName() + ")");
            }
        }
        if (!members.isEmpty()) {
            fail("Family member doesn't contains the following members : " + members.toString());
        }
    }

    @Test
    void aJsonSavedFamilyShouldContainsAllInformationsAboutTheFamily() throws MalformedURLException {

        // Given : a newly generated family
        UUID memberUUID = UUID.randomUUID();
        URL memberPicture = URI.create("https://cdn-icons-png.flaticon.com/512/3541/3541871.png").toURL();
        Identity id = new Identity(memberUUID, "memberName", memberPicture);
        Position pos = new Position(0.0, 0.0);

        FamilyMember member = new FamilyMember(id, pos);

        Set<FamilyMember> members = new HashSet<>();
        members.add(new FamilyMember(id, pos));
        Family f = new Family("famName", members);

        // When : the family is saved as json
        String saved = FamilyService.toJson(new GraphicalFamily(f));

        // Then : the json must contains all the informations about the family
        assertTrue(saved.contains("memberName"), "Saved family doesnt contains the name of the member");
        assertTrue(saved.contains(memberUUID.toString()), "Saved family doesnt contains the UUID of the member");
        assertTrue(saved.contains(memberPicture.toString()), "Saved family doesnt contains the picture URL of the member");
    }
}