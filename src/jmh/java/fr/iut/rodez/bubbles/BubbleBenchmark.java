package fr.iut.rodez.bubbles;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.service.FamilyService;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;

import java.io.File;
import java.util.List;

public class BubbleBenchmark {

    @Benchmark
    public void benchmarkShortestPath() {

        var file = new File("src/jmh/resources/family.json");

        var result = FamilyService.loadFamilyFromFile(file);

        FamilyService.FamilyLoadingResult.Loaded loaded = (FamilyService.FamilyLoadingResult.Loaded) result;

        Family family = loaded.family();

        FamilyMember sylvain = family.members().stream().filter(familyMember -> familyMember.getIdentity().getName().equals("Sylvain")).toList().getFirst();

        FamilyMember elise = family.members().stream().filter(familyMember -> familyMember.getIdentity().getName().equals("Sylvain")).toList().getFirst();

        List<FamilyMember> memberInRelation = sylvain.findPathToAnotherMemberWithFixLength(elise,family.members());

    }

}
