module fr.iut.rodez.bubbles {
    requires javafx.controls;
    requires com.fasterxml.jackson.databind;
    requires javafx.fxml;
    requires javafx.graphics;

    exports fr.iut.rodez.bubbles;
    exports fr.iut.rodez.bubbles.domain;
    exports fr.iut.rodez.bubbles.fx.graphics;
    exports fr.iut.rodez.bubbles.service to com.fasterxml.jackson.databind;
    exports fr.iut.rodez.bubbles.fx.model;
    exports fr.iut.rodez.bubbles.fx.controller;
    exports fr.iut.rodez.bubbles.memento;

    opens fr.iut.rodez.bubbles.fx.controller to javafx.graphics, javafx.fxml;
}