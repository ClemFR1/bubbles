package fr.iut.rodez.bubbles;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.fx.controller.GraphController;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.service.FamilyService;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.Loaded;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.LoadingError;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private static void onLoaded(Stage primaryStage, Family loadedFamily, Path pathToLoadedFile) {
        try {
            GraphicalFamily graphicalFamily = new GraphicalFamily(loadedFamily);
            GraphController graphController =
                    new GraphController(graphicalFamily,
                            GraphController.EventManagementStrategy.NODE_MANIPULATION,
                            pathToLoadedFile);

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/view/app.fxml"));
            fxmlLoader.setController(graphController);
            Parent root = fxmlLoader.load();

            Scene scene = new Scene(root);
            scene.setOnKeyPressed(graphController::setOnKeyPressed);

            primaryStage.setTitle("Bubbles - " + loadedFamily.name());
            primaryStage.setScene(scene);
            primaryStage.setMaximized(true);
            primaryStage.show();

            primaryStage.setOnCloseRequest(event -> {
                graphController.setOnCloseRequest();
            });

        } catch (IOException ioException) {
            onLoadingError(ioException);
        }
    }

    private static void onLoadingError(Exception cause) {
        System.out.println(cause.getMessage());
        Platform.exit();
        System.exit(1);
    }

    @Override
    public void start(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Family File...");
        File file = fileChooser.showOpenDialog(primaryStage);
        switch (FamilyService.loadFamilyFromFile(file)) {
            case Loaded loaded -> onLoaded(primaryStage, loaded.family(), file.toPath());
            case LoadingError loadingError -> onLoadingError(loadingError.cause());
        }
    }
}
