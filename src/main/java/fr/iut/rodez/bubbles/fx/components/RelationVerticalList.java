package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalRelation;
import javafx.scene.layout.VBox;

public class RelationVerticalList {

    private GraphicalFamily family;
    private VBox relationsContainer;

    public RelationVerticalList(GraphicalFamily family,
                                VBox relationsContainer) {

        this.family = family;
        this.relationsContainer = relationsContainer;

        initRelationList();
    }

    private void initRelationList() {
        relationsContainer.getChildren()
                .addAll(family.relations.stream()
                        .map(relation -> new RelationDescriptionBox(relation.source(), relation.target(),
                                relation.source().isLoveWith(relation.target()) ? "\u2764" : "\uD83D\uDC6A"))
                        .toList());

        family.relations.addListener((observable, oldValue, newValue) -> {
            relationsContainer.getChildren()
                    .clear();
            for (GraphicalRelation relation : newValue) {
                relationsContainer.getChildren()
                        .add(new RelationDescriptionBox(relation.source(), relation.target(),
                                relation.source().isLoveWith(relation.target()) ? "\u2764" : "\uD83D\uDC6A"));
            }
        });
    }

    public void updateRelationList () {
        relationsContainer.getChildren().clear();
        relationsContainer.getChildren()
                .addAll(family.relations.stream()
                        .map(relation ->
                                new RelationDescriptionBox(
                                        relation.source(),
                                        relation.target(),
                                        relation.source().isLoveWith(relation.target()) ? "\u2764" : "\uD83D\uDC6A"))
                        .toList());
    }

}
