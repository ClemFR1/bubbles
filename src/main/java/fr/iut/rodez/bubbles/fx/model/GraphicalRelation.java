package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.graphics.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public record GraphicalRelation(
        GraphicalFamilyMember source,
        GraphicalFamilyMember target
) implements Drawable {

    @Override
    public void drawWith(GraphicsContext context) {
        context.setStroke(Color.WHITE);
        context.setLineWidth(4.0);

        createArrow(source, target, context);

        if (source.isLoveWith(target)) {
            createArrow(target, source, context);
        }
    }

    /**
     * Function for calculate the distance between two points
     * @param x1 x for source
     * @param y1 y for source
     * @param x2 x for target
     * @param y2 y for target
     * @return distance between this points
     */
    private double calculateDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    /**
     * Function for calculate coordinates branches of the arrow
     * @param x1 x for source
     * @param y1 y for source
     * @param x2 x for target
     * @param y2 y for target
     * @param length of arrow
     * @param angleRad of arrow
     * @return the points of arrow
     */
    private double[] calculateArrowhead(double x1, double y1, double x2, double y2, double length, double angleRad) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double distance = calculateDistance(x1, y1, x2, y2);
        double nx = dx / distance;
        double ny = dy / distance;
        double arrowX1 = x2 - length * (nx * Math.cos(angleRad) - ny * Math.sin(angleRad));
        double arrowY1 = y2 - length * (nx * Math.sin(angleRad) + ny * Math.cos(angleRad));
        double arrowX2 = x2 - length * (nx * Math.cos(-angleRad) - ny * Math.sin(-angleRad));
        double arrowY2 = y2 - length * (nx * Math.sin(-angleRad) + ny * Math.cos(-angleRad));
        return new double[]{arrowX1, arrowY1, arrowX2, arrowY2};
    }

    private void createArrow(GraphicalFamilyMember source, GraphicalFamilyMember target, GraphicsContext context){

        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();

        double circleRadius = 20;
        double arrowLength = 20;
        double angleRad = Math.PI / 6;

        String love = "\u2764";
        String parent = "\uD83D\uDC6A";

        double sourceX = sourcePosition.x();
        double sourceY = sourcePosition.y();
        double targetX = targetPosition.x();
        double targetY = targetPosition.y();

        double middleX = (sourceX + targetX)/2;
        double middleY = (sourceY + targetY)/2;

        double oldSize = context.getFont().getSize();
        context.setFont(Font.font(context.getFont().getName(), 20.0));
        if (source.isLoveWith(target)) {
            context.fillText(love, middleX, middleY);
        } else {
            context.fillText(parent, middleX, middleY);
        }
        context.setFont(Font.font(context.getFont().getName(), oldSize));

        // Calculation of the direction vector from source to target
        double dx = targetX - sourceX;
        double dy = targetY - sourceY;

        // Vector normalization
        double length = Math.sqrt(dx * dx + dy * dy);
        dx /= length;
        dy /= length;

        // Calculation of the new target position
        double newTargetX = targetX - dx * (circleRadius + arrowLength);
        double newTargetY = targetY - dy * (circleRadius + arrowLength);

        double[] arrowHead = calculateArrowhead(sourceX, sourceY, newTargetX, newTargetY, arrowLength, angleRad);

        double arrowX1 = arrowHead[0];
        double arrowY1 = arrowHead[1];
        double arrowX2 = arrowHead[2];
        double arrowY2 = arrowHead[3];

        context.strokeLine(sourcePosition.x(), sourcePosition.y(), targetPosition.x(), targetPosition.y());
        context.strokeLine(newTargetX, newTargetY, arrowX1, arrowY1);
        context.strokeLine(newTargetX, newTargetY, arrowX2, arrowY2);
    }

    @Override
    public boolean covers(Position position) {
        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();
        double x = position.x() - sourcePosition.x();
        double y = position.y() - sourcePosition.y();
        double a = targetPosition.x() - sourcePosition.x();
        double b = targetPosition.y() - sourcePosition.y();
        double t = (x * a + y * b) / (a * a + b * b);
        double xProj = sourcePosition.x() + t * a;
        double yProj = sourcePosition.y() + t * b;
        return position.distanceTo(new Position(xProj, yProj)) <= 4.0;
    }
}
