package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.domain.Social;
import fr.iut.rodez.bubbles.fx.graphics.Draggable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.net.URL;

public class GraphicalFamilyMember implements Draggable, Social<GraphicalFamilyMember> {

    private static final double CIRCLE_RADIUS = 32.0;

    private final FamilyMember familyMember;

    public GraphicalFamilyMember(FamilyMember familyMember) {
        this.familyMember = familyMember;
    }

    public Position currentPosition() {
        return familyMember.currentPosition();
    }

    public Identity identity() {
        return familyMember.identity;
    }

    @Override
    public void drawWith(GraphicsContext context) {
        Position pos = currentPosition();

        context.setFill(Color.GREY);
        context.setLineWidth(10.0);

        context.fillOval(pos.x() - CIRCLE_RADIUS, pos.y() - CIRCLE_RADIUS,
                CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2);

        if (familyMember.isSelected()) {
            context.setStroke(Color.DODGERBLUE);
            context.setFill(Color.DODGERBLUE);
        } else {
            context.setStroke(Color.WHITE);
            context.setFill(Color.WHITE);
        }
        context.strokeOval(pos.x() - CIRCLE_RADIUS, pos.y() - CIRCLE_RADIUS,
                CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2);
        context.fillText(identity().getName(), pos.x() - (CIRCLE_RADIUS / 2),
                pos.y() + CIRCLE_RADIUS * 2);
    }

    @Override
    public boolean covers(Position position) {
        return currentPosition().distanceTo(position) <= CIRCLE_RADIUS;
    }

    @Override
    public void moveTo(Position position) {
        familyMember.moveTo(position);
    }

    @Override
    public void parentOf(GraphicalFamilyMember other) {
        familyMember.parentOf(other.familyMember);
    }

    public FamilyMember getFamilyMember() {
        return familyMember;
    }

    public boolean isParentOf(GraphicalFamilyMember other) {
        return familyMember.isParentOf(other.familyMember);
    }
    public boolean isLoveWith(GraphicalFamilyMember other) {
        return familyMember.isLoveWith(other.familyMember);
    }

    public void setSelected(boolean selected) {
        familyMember.setSelected(selected);
    }

    public boolean isSelected() {
        return familyMember.isSelected();
    }

    public void deleteAllRelations() {
        familyMember.deleteAllRelations();
    }

    public void modifMember(String newName, URL newUrl) {

        String foreignName = getFamilyMember().getIdentity().getName();
        URL foreignUrl = getFamilyMember().getIdentity().getPicture();

        getFamilyMember().getIdentity().setName(newName == null ? foreignName : newName);
        getFamilyMember().getIdentity().setPicture(newUrl == null ? foreignUrl : newUrl);
    }

    @Override
    public void inLoveWith(GraphicalFamilyMember other) {
        familyMember.inLoveWith(other.familyMember);
    }
}
