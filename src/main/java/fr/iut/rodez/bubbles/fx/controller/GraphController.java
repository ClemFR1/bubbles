package fr.iut.rodez.bubbles.fx.controller;

import fr.iut.rodez.bubbles.domain.*;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.components.RelationVerticalList;
import fr.iut.rodez.bubbles.memento.Caretaker;
import fr.iut.rodez.bubbles.memento.GraphicalFamilySnapshot;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;

import fr.iut.rodez.bubbles.service.FamilyService;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static fr.iut.rodez.bubbles.fx.controller.GraphController.EventManagementStrategy.*;

public class GraphController {

    @FXML
    private Label nameOfMember, nameToOmputeRelationTo;
    @FXML
    private TextField textFieldName,textFieldURL, textFieldNameInRelationWith;
    @FXML
    private Button buttonValidate, buttonCancel, validateRelationButton, cancelButtonRelation;
    @FXML
    private Canvas graphCanvas;
    @FXML
    private Pane graphPane;
    @FXML
    private ToggleGroup buttonBar;
    @FXML
    private ToggleButton selectButton, manipulationButton, addRelationButton, addMemberButton;
    @FXML
    private Text actionForBoxTitle;
    @FXML
    private Button deleteButton;
    @FXML
    private Button updateButton,computeRelationButton;

    @FXML
    private VBox relationContainer;
    @FXML
    private Button btnLover, btnParent, btnCancel;

    private final GraphicalFamily family;
    private final Path pathToLoadedFile;
    private final Caretaker<GraphicalFamilySnapshot, GraphicalFamily> caretaker;

    private final List<GraphicalFamilyMember> selectedMembers;

    // Current graphical item that is selected
    private GraphicalFamilyMember activeSelection;

    private GraphicsContext context;

    private EventManagementStrategy eventManagementStrategy;

    private Position point;

    // The vertical list containing the relationships between members
    private RelationVerticalList relationVerticalList;

    private boolean onAddRelation = false;

    public GraphController(GraphicalFamily family, EventManagementStrategy eventManagementStrategy, Path pathToLoadedFile) {

        Objects.requireNonNull(family, "family must not be null");
        Objects.requireNonNull(eventManagementStrategy, "event strategy must not be null");

        this.family = family;
        this.pathToLoadedFile = pathToLoadedFile;
        this.eventManagementStrategy = eventManagementStrategy;
        this.selectedMembers = new ArrayList<>();

        Path savePath = Path.of("./memento_bubbles").resolve(pathToLoadedFile.getFileName());
        this.caretaker = new Caretaker<>(
                family,
                savePath,
                GraphicalFamilySnapshot.class
        );
    }

    @FXML
    void initialize() {
        // Because the initialize function is called when creating the window
        // and when creating popups, we only run this code when the window
        // is created by checking if we already have context

        if (context == null) {
            context = graphCanvas.getGraphicsContext2D();

            graphCanvas.heightProperty().bind(graphPane.heightProperty());
            graphCanvas.widthProperty().bind(graphPane.widthProperty());
            graphCanvas.heightProperty().addListener(evt -> draw());
            graphCanvas.widthProperty().addListener(evt -> draw());
            graphCanvas.setOnKeyPressed(this::setOnKeyPressed);

            relationVerticalList = new RelationVerticalList(family, relationContainer);
            setButtonBarListener();

            // Save the initial state of the window
            caretaker.saveState();
        }

    }

    public void setOnKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.Z) {
            System.out.println("UNDO !");
            caretaker.undo();
            relationVerticalList.updateRelationList();
            draw();
        } else if (keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.S) {
            try {
                Files.write(pathToLoadedFile, FamilyService.toJson(family).getBytes());
                System.out.println("saved file !");
            } catch (IOException ex) {
                System.out.println("Cannot save file");
                ex.printStackTrace();
            }
        }
    }

    @FXML
    private void setOnMouseDragged(MouseEvent event) {
        Position position = new Position(event.getX(), event.getY());
        switch (eventManagementStrategy) {
            case NODE_MANIPULATION -> moveSelectedElementsTo(position);
            case EDGE_MANIPULATION -> definePointByReachableElement(position);
            case SELECTION_MODE -> {}
            case ADD_MEMBER -> {}
        }
        event.consume();
    }

    @FXML
    private void setOnMousePressed(MouseEvent event) throws Exception {
        Position position = new Position(event.getX(), event.getY());
        switch (eventManagementStrategy) {
            case NODE_MANIPULATION, EDGE_MANIPULATION -> selectFirstElementPositionedOn(position);
            case SELECTION_MODE -> selectOneNode(position);
            case ADD_MEMBER -> showPopupAddingMember(position,getStage(event));
        }
        event.consume();
    }

    @FXML
    private void setOnMouseReleased(MouseEvent event) throws IOException {
        Position position = new Position(event.getX(), event.getY());
        switch (eventManagementStrategy) {
            case NODE_MANIPULATION -> {
                if (!selectedMembers.isEmpty()) {
                    caretaker.saveState();
                }
                clearSelection();
            }
            case EDGE_MANIPULATION -> selectedAsParentsOfPotentialOne(position, event);
            case SELECTION_MODE -> {}
            case ADD_MEMBER -> {}
        }
        event.consume();
    }

    @FXML
    private void setOnMouseExited(MouseEvent event) {
        switch (eventManagementStrategy) {
            case NODE_MANIPULATION -> clearSelection();
            case EDGE_MANIPULATION -> clearSelectionAndPoint();
            case SELECTION_MODE -> {}
            case ADD_MEMBER -> {}
        }
        event.consume();
    }

    @FXML
    private void setOnDeleteAction() {
        if (activeSelection != null) {
            family.deleteOneMember(activeSelection);
            activeSelection = null;
            draw();
            relationVerticalList.updateRelationList();

            //Disable actions for section
            deleteButton.setDisable(true);
            updateButton.setDisable(true);
            computeRelationButton.setDisable(true);
            actionForBoxTitle.setText("Actions for ...");
            caretaker.saveState();
        }
    }

    @FXML
    private void setOnUpdateAction(Event event) throws Exception {
        showPopupUpdatingMember(getStage(event));
    }

    @FXML
    private void setOnRelationAction(Event event) throws Exception {
        showPopupComputeRelation(getStage(event));
    }

    private void moveSelectedElementsTo(Position position) {
        if (!selectedMembers.isEmpty()) {
            selectedMembers.forEach(element -> element.moveTo(position));
            draw();
        }
    }

    private void selectFirstElementPositionedOn(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(element -> {
            selectedMembers.clear();
            selectedMembers.add(element);
        });
    }

    private void selectOneNode(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(element -> {
            if (activeSelection != null) {
                activeSelection.setSelected(false);
            }
            activeSelection = element;
            activeSelection.setSelected(true);
            actionForBoxTitle.setText("Actions for " + activeSelection.identity().getName());
            deleteButton.setDisable(false);
            updateButton.setDisable(false);
            computeRelationButton.setDisable(false);
            draw();
        });
        relationVerticalList.updateRelationList();
    }

    private void definePointByReachableElement(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresentOrElse(member -> {
            Position memberPosition = member.currentPosition();
            point = new Position(memberPosition.x(), memberPosition.y());
        }, () -> point = position);
        draw();
    }

    private void selectedAsParentsOfPotentialOne(Position position, Event event) throws IOException {

        GraphicalFamilyMember member = firstFamilyMemberPositionedOn(position).orElse(null);
        if (member != null){

            onAddRelation = true;

            List<GraphicalFamilyMember> lSelectedMembers = selectedMembers;

            Popup popupAddRelation = new Popup();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/popUpAddRelation.fxml"));
            fxmlLoader.setController(this);

            popupAddRelation.getContent().add(fxmlLoader.load());

            Stage stage;
            stage = (Stage) ((Node) (event.getSource())).getScene().getWindow();

            btnLover.setOnAction(mouseEvent -> {
                for (GraphicalFamilyMember other : lSelectedMembers) {
                    family.createRelation(other, member, false, true);
                }
                System.out.println("lover");
                popupAddRelation.hide();
                onAddRelation = false;
                caretaker.saveState();
                clearSelectionAndPoint();
            });

            btnParent.setOnAction(mouseEvent -> {
                for (GraphicalFamilyMember other : lSelectedMembers) {
                    family.createRelation(other, member, true, false);
                }
                System.out.println("parent");
                popupAddRelation.hide();
                onAddRelation = false;
                caretaker.saveState();
                clearSelectionAndPoint();
            });

            btnCancel.setOnAction(mouseEvent -> {
                System.out.println("cancel");
                popupAddRelation.hide();
                onAddRelation = false;
                clearSelectionAndPoint();
            });

            popupAddRelation.show(stage);

        } else {
            System.out.println("member vide");
            onAddRelation = false;
            clearSelectionAndPoint();
        }

    }

    private void clearSelection() {
        selectedMembers.clear();
        draw();
    }

    private void clearSelectionAndPoint() {
        if (!onAddRelation){
            selectedMembers.clear();
            point = null;
            draw();
        }
    }

    private boolean isTemporaryLineToDraw() {
        return eventManagementStrategy == EDGE_MANIPULATION && point != null && !selectedMembers.isEmpty();
    }

    private void showPopupAddingMember(Position position,Stage stage) throws Exception {
        AtomicBoolean created = new AtomicBoolean(false);
        Popup popupAddMember = new Popup();
        FXMLLoader fxmlLoader = new FXMLLoader(GraphController.class.getResource("/view/popup_add_member.fxml"));
        fxmlLoader.setController(this);
        popupAddMember.getContent().add(fxmlLoader.load());

        buttonCancel.setOnMouseClicked(mouseEvent -> popupAddMember.hide());
        buttonValidate.setOnMouseClicked(mouseEvent -> {
            try {
                created.set(addMemberToGraph(position));
            } catch (IOException | URISyntaxException e) {
                throw new RuntimeException(e);
            }
            if(created.get()){
                popupAddMember.hide();
            }
        });
        popupAddMember.show(stage);
    }

    private void showPopupUpdatingMember(Stage stage) throws Exception {
        Popup popupUpdatingMember = new Popup();
        FXMLLoader fxmlLoader = new FXMLLoader(GraphController.class.getResource("/view/popup_add_member.fxml"));
        fxmlLoader.setController(this);
        popupUpdatingMember.getContent().add(fxmlLoader.load());
        nameOfMember.setText("updating " + activeSelection.getFamilyMember().getIdentity().getName());
        buttonCancel.setOnMouseClicked(mouseEvent -> popupUpdatingMember.hide());
        buttonValidate.setOnMouseClicked(mouseEvent -> {
            URL urlPicture;
            try {
                urlPicture = new URI(textFieldURL.getText()).toURL();
            } catch (Exception e) {
                urlPicture = null;
            }
            String name = textFieldName.getText();
            if (family.verifyExistingName(name) != 0) {
                long numbersOfNameExisting = family.verifyExistingName(name);
                do {
                    name += " (" + numbersOfNameExisting + ")";
                } while (family.nameAlreadyExist(name));
            }
            activeSelection.modifMember(name, urlPicture);
            draw();
            relationVerticalList.updateRelationList();
            popupUpdatingMember.hide();
            caretaker.saveState();
        });
        popupUpdatingMember.show(stage);
    }

    private void showPopupComputeRelation(Stage stage) throws Exception {
        Popup popupComputeRelation = new Popup();
        FXMLLoader fxmlLoader = new FXMLLoader(GraphController.class.getResource("/view/popup_compute_relation.fxml"));
        fxmlLoader.setController(this);
        popupComputeRelation.getContent().add(fxmlLoader.load());
        nameToOmputeRelationTo.setText("find the relation path between " + activeSelection.getFamilyMember().getIdentity().getName() + "and the name entered");
        cancelButtonRelation.setOnMouseClicked(mouseEvent -> popupComputeRelation.hide());
        validateRelationButton.setOnMouseClicked(mouseEvent -> {
            if (textFieldNameInRelationWith.getText().isEmpty()){
                textFieldNameInRelationWith.setStyle("-fx-background-color: rgba(203,14,14,0.67)");
            } else {
                //finding the GraphicalFamilyMember related to the name given
                try {
                    GraphicalFamilyMember memberInRelationWith =
                            family.members.stream()
                                    .filter(members -> members.getFamilyMember().getIdentity().getName().equals(textFieldNameInRelationWith.getText()))
                                    .toList().getFirst();
                    activeSelection.getFamilyMember().findShortestRelationWith(memberInRelationWith.getFamilyMember());
                    popupComputeRelation.hide();
                }catch (NoSuchElementException e){
                    //case where the name given don't exist in the family
                    textFieldNameInRelationWith.setStyle("-fx-background-color: rgba(203,14,14,0.67)");
                }
            }

        });
        popupComputeRelation.show(stage);
    }

    private boolean addMemberToGraph(Position position) throws IOException, URISyntaxException {
        String name = textFieldName.getText();
        URL urlPictur;
        try {
            urlPictur = new URI(textFieldURL.getText()).toURL();
        } catch (Exception e) {
            urlPictur = null;
        }
        if (name.isEmpty()){
            textFieldName.setStyle("-fx-background-color: rgba(203,14,14,0.67)");
        } else {
            long numbersOfNameExisting = family.verifyExistingName(name);
            if (family.verifyExistingName(name) != 0) {
                do {
                    name += " (" + numbersOfNameExisting + ")";
                } while (family.nameAlreadyExist(name));
            }
            Identity memberIdentity = new Identity(UUID.randomUUID(),name,urlPictur);
            FamilyMember newMember = new FamilyMember(memberIdentity,position);
            GraphicalFamilyMember newGraphicalMember = new GraphicalFamilyMember(newMember);
            family.members.add(newGraphicalMember);
            family.addingOneMember(newGraphicalMember);
            caretaker.saveState();
            draw();
            return true;
        }
        return false;
    }

    private Optional<GraphicalFamilyMember> firstFamilyMemberPositionedOn(Position position) {
        return family.members
                .stream()
                .filter(member -> member.covers(position))
                .findFirst();
    }

    private static Stage getStage(Event event) {
        return (Stage) ((Node) (event.getSource())).getScene().getWindow();
    }

    protected void draw() {
        context.clearRect(0, 0,
                graphCanvas.getWidth(),
                graphCanvas.getHeight());

        family.relations.forEach(relation -> relation.drawWith(context));

        if (isTemporaryLineToDraw() && !selectedMembers.isEmpty()) {
            var firstElement = selectedMembers.getFirst();
            var position = firstElement.currentPosition();
            context.setLineWidth(4.0);
            context.setStroke(Color.GREY);
            context.strokeLine(point.x(), point.y(), position.x(), position.y());
        }

        family.members.forEach(member -> member.drawWith(context));
    }

    private void setButtonBarListener() {
        buttonBar.selectedToggleProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue == manipulationButton) {
                        eventManagementStrategy = NODE_MANIPULATION;
                    } else if (newValue == addRelationButton) {
                        eventManagementStrategy = EDGE_MANIPULATION;
                    } else if (newValue == selectButton) {
                        eventManagementStrategy = SELECTION_MODE;
                    } else if (newValue == addMemberButton) {
                        eventManagementStrategy = ADD_MEMBER;
                    } else if (newValue == computeRelationButton) {
                        eventManagementStrategy = COMPUTE_RELATION;
                    } else {
                            buttonBar.selectToggle(oldValue);
                        }
                    });
    }

    public void setOnCloseRequest() {
        caretaker.forceSaveCurrentHistoryToFile();
        try {
            Files.write(pathToLoadedFile, FamilyService.toJson(family).getBytes());
        } catch (IOException ex) {
            System.out.println("Erreur lors de l'enregistrement !");
            ex.printStackTrace();
        }
    }

    public enum EventManagementStrategy {
        NODE_MANIPULATION, EDGE_MANIPULATION, SELECTION_MODE, ADD_MEMBER, COMPUTE_RELATION
    }
}
