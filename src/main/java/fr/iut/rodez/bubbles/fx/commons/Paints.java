package fr.iut.rodez.bubbles.fx.commons;

import javafx.scene.paint.*;

public final class Paints {

    public static final Paint GLASS = Color.rgb(255, 255, 255, 0.45);

    private Paints() {}

}
