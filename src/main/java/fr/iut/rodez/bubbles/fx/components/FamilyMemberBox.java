package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import java.util.List;

public class FamilyMemberBox extends HBox {

    private static final int DEFAULT_SPACING = 8;

    public FamilyMemberBox(GraphicalFamilyMember familyMember) {
        this(familyMember, Orientation.AVATAR_ON_LEFT);
    }

    public FamilyMemberBox(GraphicalFamilyMember familyMember, Orientation orientation) {
        super(DEFAULT_SPACING);
        setAlignment(Pos.CENTER_LEFT);

        Identity identity = familyMember.identity();

        final GlassText name;
        if (familyMember.isSelected()) {
            name = new GlassText(identity.getName(), Color.DODGERBLUE);
        } else {
            name = new GlassText(identity.getName());
        }

        final Avatar avatar = new Avatar(identity.getPicture());

        getChildren().addAll(switch (orientation) {
            case AVATAR_ON_LEFT -> List.of(avatar, name);
            case AVATAR_ON_RIGHT -> List.of(name, avatar);
        });
    }

    public enum Orientation {
        AVATAR_ON_LEFT, AVATAR_ON_RIGHT
    }
}
