package fr.iut.rodez.bubbles.fx.commons;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;

public final class Backgrounds {

    private static final BackgroundFill GLASS_FILL = new BackgroundFill(Paints.GLASS, CornerRadii.EMPTY, Insets.EMPTY);
    public static final Background GLASS = new Background(GLASS_FILL);

    private Backgrounds() {}
}
