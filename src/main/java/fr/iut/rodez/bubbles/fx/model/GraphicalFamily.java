package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.Relation;
import fr.iut.rodez.bubbles.memento.GraphicalFamilySnapshot;
import fr.iut.rodez.bubbles.memento.Originator;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.util.*;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toMap;

public class GraphicalFamily implements Originator<GraphicalFamily, GraphicalFamilySnapshot> {

    private Family family;

    public final ListProperty<GraphicalFamilyMember> members;
    public final ListProperty<GraphicalRelation> relations;
    public final String name;

    public GraphicalFamily(Family family) {
        this.family = family;
        this.name = family.name();

        Map<UUID, GraphicalFamilyMember> membersById = family.members()
                                                             .stream()
                                                             .collect(toMap(f -> f.identity.getId(), GraphicalFamilyMember::new));

        this.members = new SimpleListProperty<>(FXCollections.observableArrayList(membersById.values()));
        this.relations = new SimpleListProperty<>(FXCollections.observableArrayList());

        List<String[]> createdLoveRelations = new ArrayList<>();

        family.members()
              .forEach(member -> {
                  Set<Relation> relations = member.relations();
                  relations.forEach(relation -> {
                      GraphicalFamilyMember source = membersById.get(member.identity.getId());
                      GraphicalFamilyMember target = membersById.get(relation.related().identity.getId());
                      if (source.isParentOf(target)) {
                          createRelationInitialisation(source, target);
                      } else if (source.isLoveWith(target)) {

                          // Checking that the love relation isn't created yet
                          for (String[] rel : createdLoveRelations) {
                              if (rel[0].equals(source.identity().getName()) || rel[0].equals(target.identity().getName())
                                  && rel[1].equals(source.identity().getName()) || rel[1].equals(target.identity().getName())) {
                                  return;
                              }
                          }

                          createdLoveRelations.add(new String[]{source.identity().getName(), target.identity().getName()});
                          createRelationInitialisation(source, target);
                      }
                  });
              });
    }

    public void createRelation(GraphicalFamilyMember source, GraphicalFamilyMember target, boolean parent, boolean lover) {

        if(source != target) {
            if(parent){
                source.parentOf(target);
            } else if (lover) {
                source.inLoveWith(target);
            }
        }

        relations.add(new GraphicalRelation(source, target));
    }

    public void createRelationInitialisation(GraphicalFamilyMember source, GraphicalFamilyMember target) {
        relations.add(new GraphicalRelation(source, target));
    }

    public void deleteOneMember(GraphicalFamilyMember graphicalFamilyMember) {
        //Delete the member on the family
        family.deleteMember(graphicalFamilyMember.getFamilyMember());
        //Delete all the concretes relations
        graphicalFamilyMember.deleteAllRelations();

        //Delete graphically the member
        // and all the relations associate to this member
        members.remove(graphicalFamilyMember);
        relations.removeIf(graphicalRelation ->
                graphicalRelation.source().equals(graphicalFamilyMember)
                | graphicalRelation.target().equals(graphicalFamilyMember));
    }

    public void addingOneMember(GraphicalFamilyMember graphicalFamilyMember) {
        family.addingMember(graphicalFamilyMember.getFamilyMember());
    }

    public boolean nameAlreadyExist(String name){
        return members.stream()
                .anyMatch(member -> member.getFamilyMember().getIdentity().getName().equals(name));
    }

    public long verifyExistingName(String name){
        List<GraphicalFamilyMember> numberMatchingNames;
        String regexName = "^" + name.toLowerCase() + "( \\(\\d+\\))?$";
        Pattern p = Pattern.compile(regexName);

        numberMatchingNames = members
                .stream()
                .filter(member -> p.matcher(member.getFamilyMember().getIdentity().getName().toLowerCase()).matches())
                .toList();

        return numberMatchingNames.size();
    }

    @Override
    public GraphicalFamilySnapshot createSnapshot() {
        return new GraphicalFamilySnapshot(this, this);
    }

    @Override
    public void setState(GraphicalFamily newState) {
        members.clear();
        relations.clear();
        members.addAll(newState.members);
        relations.addAll(newState.relations);
    }
}
