package fr.iut.rodez.bubbles.fx.components;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class GlassText extends Text {

    public GlassText(String text) {
        this(text, Color.WHITE);
    }

    public GlassText(String text, Color color) {
        super(text);
        setFill(color);
        setFont(Font.font("Arial", FontWeight.BLACK, 14));
    }
}
