package fr.iut.rodez.bubbles.domain;

import java.net.URL;
import java.util.UUID;

public class Identity {

    final private UUID id;
    private String name;
    private URL picture;

    public Identity(UUID id, String name, URL picture) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URL getPicture() {
        return picture;
    }

    public void setPicture(URL picture) {
        this.picture = picture;
    }
}
