package fr.iut.rodez.bubbles.domain;

public final class InLoveWith extends Relation {

    public InLoveWith(FamilyMember other) {
        super(other);
    }
}
