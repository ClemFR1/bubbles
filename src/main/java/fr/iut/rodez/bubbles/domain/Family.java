package fr.iut.rodez.bubbles.domain;

import java.util.Set;

public record Family(
        String name,
        Set<FamilyMember> members
) {
    public Set<FamilyMember> members() {
        return Set.copyOf(members); // defensive copy
    }

    public void deleteMember(FamilyMember familyMember) {
        members.remove(familyMember);
    }

    public void addingMember(FamilyMember familyMember) {
        members.add(familyMember);
    }
}
