package fr.iut.rodez.bubbles.domain;

public sealed abstract class Relation permits ChildRelation, InLoveWith, ParentRelation {

    private final FamilyMember related;

    protected Relation(FamilyMember related) {
        this.related = related;
    }

    public FamilyMember related() {
        return related;
    }
}