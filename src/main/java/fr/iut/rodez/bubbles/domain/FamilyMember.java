package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.utils.Validations;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyMember implements Social<FamilyMember> {

    public final Identity identity;

    private Position position;

    private boolean selected;

    private final HashSet<Relation> relations;

    public FamilyMember(Identity identity, Position position) {
        this.identity = Objects.requireNonNull(identity, "Identity must not be null.");
        this.position = Objects.requireNonNull(position, "Position must not be null.");
        this.relations = new HashSet<>();
        selected = false;
    }

    public Set<Relation> relations() {
        return Set.copyOf(relations);
    }

    public Set<FamilyMember> lovedOnes() {
        return lovedOnesAsStream().collect(Collectors.toSet());
    }

    public Set<FamilyMember> children() {
        return relations
                .stream()
                .filter(ChildRelation.class::isInstance)
                .map(Relation::related)
                .collect(Collectors.toSet());
    }

    private Stream<FamilyMember> lovedOnesAsStream() {
        return relations
                .stream()
                .map(Relation::related);
    }

    public Set<FamilyMember> relatives(int maxDegree) {
        Validations.requireStrictlyPositive(maxDegree, () -> "Max degree must be at least 1.");

        Set<FamilyMember> relatives = new HashSet<>();
        Collection<FamilyMember> familyMembersAtNextDegree = List.of(this);

        for (int degree = 1; degree <= maxDegree; degree++) {

            familyMembersAtNextDegree = familyMembersAtNextDegree
                    .stream()
                    .flatMap(FamilyMember::lovedOnesAsStream)
                    .filter(familyMember -> familyMember != this && !relatives.contains(familyMember))
                    .toList();

            relatives.addAll(familyMembersAtNextDegree);
        }

        return relatives;
    }

    public List<FamilyMember> findPathToAnotherMemberWithFixLength(FamilyMember toFind, Set<FamilyMember> setOfMemberToCheck){

        Queue<List<FamilyMember>> allPath = new LinkedList<>();

        List<FamilyMember> initialPath = new ArrayList<>();
        initialPath.add(this);
        allPath.add(initialPath);

        Set<FamilyMember> alreadyVisited = new HashSet<>();
        alreadyVisited.add(this);

        while (!allPath.isEmpty()){
            List<FamilyMember> path = allPath.poll();
            FamilyMember curentlyVisited = path.getLast();

            if(curentlyVisited == toFind){
                return path;
            }

            for(Relation inRelation : curentlyVisited.relations){
                if(!alreadyVisited.contains(inRelation.related())){
                    alreadyVisited.add(inRelation.related());
                    List<FamilyMember> newPath = new ArrayList<>(path);
                    newPath.add(inRelation.related());
                    allPath.add(newPath);
                }
            }
        }
        return new LinkedList<>();
    }

    public void findShortestRelationWith(FamilyMember memberRelated){

        //finding how many moves are in the shortest link between this and the memberRelated
        Set<FamilyMember> relatedTo = new HashSet<>();
        Set<FamilyMember> foreignRelatedTo = new HashSet<>();
        int degree = 0;
        do{
            foreignRelatedTo = relatedTo;
            degree++;
            relatedTo = relatives(degree);
        }while (!relatedTo.contains(memberRelated) && !relatedTo.equals(foreignRelatedTo));
        if (relatedTo.contains(memberRelated)){
            List<FamilyMember> pathOfMembers = findPathToAnotherMemberWithFixLength(memberRelated,relatedTo);
            String pathTexte = "";
            for (int nextMember = 0; nextMember < pathOfMembers.size() - 1; nextMember++) {
                if (pathOfMembers.get(nextMember).isParentOf(pathOfMembers.get(nextMember + 1))) {
                    pathTexte += pathOfMembers.get(nextMember).getIdentity().getName() + " parent of " + pathOfMembers.get(nextMember + 1).getIdentity().getName() + "\n";
                }else if (pathOfMembers.get(nextMember).isChildOf(pathOfMembers.get(nextMember + 1))){
                    pathTexte += pathOfMembers.get(nextMember).getIdentity().getName() + " child of " + pathOfMembers.get(nextMember + 1).getIdentity().getName() + "\n";
                }else {
                    pathTexte += pathOfMembers.get(nextMember).getIdentity().getName() + " in love with " + pathOfMembers.get(nextMember + 1).getIdentity().getName() + "\n";
                }
            }
            Alert box = new Alert(Alert.AlertType.INFORMATION);
            box.setTitle("relation between " + this.getIdentity().getName() + " and " + memberRelated.getIdentity().getName());
            box.setHeaderText("");
            box.setContentText(pathTexte);
            ButtonType btnOk = new ButtonType("ok", ButtonBar.ButtonData.OK_DONE);
            box.getButtonTypes().set(0,btnOk);
            box.show();

        }else {
            Alert box = new Alert(Alert.AlertType.ERROR);
            box.setTitle("relation between " + this.getIdentity().getName() + " and " + memberRelated.getIdentity().getName());
            box.setHeaderText("");
            box.setContentText("The two members aren't connected");
            ButtonType btnOk = new ButtonType("ok", ButtonBar.ButtonData.OK_DONE);
            box.getButtonTypes().set(0,btnOk);
            box.show();
        }
    }

    public Position currentPosition() {
        return position;
    }

    public void moveTo(Position position) {
        this.position = position;
    }

    @Override
    public void parentOf(FamilyMember other) {
        relations.add(new ChildRelation(other));
        other.relations.add(new ParentRelation(this));
    }

    /**
     * Determine if a family member is parent of another one
     * @param other another family member
     * @return true if this is parent of the other family member
     */
    public boolean isParentOf(FamilyMember other) {

        for (Relation relation : relations) {
            if (relation.getClass() == ChildRelation.class && relation.related().equals(other)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if a family member is in love with another one
     * @param other another family member
     * @return true if this is in love with the other family member
     */
    public boolean isLoveWith(FamilyMember other) {

        for (Relation relation : relations) {
            if (relation.getClass() == InLoveWith.class && relation.related().equals(other)) {
                return true;
            }
        }
        return false;
    }

    public boolean isChildOf(FamilyMember other) {
        for (Relation relation : relations) {
            if (relation.getClass() == ParentRelation.class && relation.related().equals(other)) {
                return true;
            }
        }
        return false;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    /**
     * Remove all the relation associated to this
     */
    public void deleteAllRelations() {
        for (Relation relation: relations) {
            relation.related().deleteOneRelation(this);
        }
        relations.clear();
    }

    /**
     * Remove a relation between this and another member
     * @param otherMember
     */
    public void deleteOneRelation(FamilyMember otherMember) {
        relations.removeIf(relation -> relation.related().equals(otherMember));
    }

    public Position getPosition() {
        return position;
    }

    public HashSet<Relation> getRelations() {
        return relations;
    }

    public Identity getIdentity() {
        return identity;
    }
    @Override
    public void inLoveWith(FamilyMember other) {
        relations.add(new InLoveWith(other));
        other.relations.add(new InLoveWith(this));
    }
}
