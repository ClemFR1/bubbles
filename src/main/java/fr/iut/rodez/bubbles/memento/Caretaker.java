package fr.iut.rodez.bubbles.memento;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;

/**
 * Caretaker class that manages the history of the mementos
 * @param <SNAPSHOTS> Type of the memento snapshot
 * @param <ORIGINATOR> Type of the originator / object that creates and receive
 *           the memento snapshot
 */
public class Caretaker<SNAPSHOTS extends MementoSnapshot<ORIGINATOR>, ORIGINATOR extends Originator<ORIGINATOR, SNAPSHOTS>> {

    public static String FILENAME = "memento_history_%d";

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(SNAKE_CASE);
    }

    private final Deque<SNAPSHOTS> history;
    private final Originator<ORIGINATOR, SNAPSHOTS> originator;
    private final Path basePath;
    private final Class<SNAPSHOTS> concreteSnapshotClass;
    private SNAPSHOTS lastSavedMemento;

    private boolean isRestoring;
    private boolean isSaving;
    private int availableFiles;

    private static final int MAX_DEQUE_SIZE = 50;
    private static final int NUMBER_SAVED_ITEMS = 30;
    private static final int MIN_DEQUE_SIZE = 15;

    public Caretaker(Originator<ORIGINATOR, SNAPSHOTS> originator,
                     Path basePath,
                     Class<SNAPSHOTS> concreteSnapshotClass) {
        this.history = new ConcurrentLinkedDeque<>();
        this.originator = originator;
        this.basePath = basePath;
        this.concreteSnapshotClass = concreteSnapshotClass;

        // Check if save folder exists
        if (!Files.exists(basePath)) {
            try {
                Files.createDirectories(basePath);
            } catch (IOException e) {
                isSaving = false;
                e.printStackTrace();
            }
        }

        isRestoring = false;
        isSaving = false;
        availableFiles = getNumberOfAvailableFileToRestoreStates();

        // Restore old session from disk
        restoreLastSavedMementosFromDisk();
    }

    public void saveState() {
        System.out.println("saving state");
        if (lastSavedMemento != null) {
            history.add(lastSavedMemento);
        }
        lastSavedMemento = originator.createSnapshot();

        // if the history is too long, we save the oldest elements to disk
        if (history.size() > MAX_DEQUE_SIZE && !isSaving) {
            isSaving = true;

            // collecting first 100 elements from the deque
            Collection<SNAPSHOTS> toSave = Collections.synchronizedList(new ArrayList<>());
            history.stream()
                    .limit(NUMBER_SAVED_ITEMS)
                    .iterator()
                    .forEachRemaining(mementoSnapshot -> {
                toSave.add(mementoSnapshot);
                history.remove(mementoSnapshot);
            });
            saveToDisk(toSave);
        }
    }

    public void undo() {
        if (!history.isEmpty()) {
            lastSavedMemento = history.removeLast();
            lastSavedMemento.restore();
        }

        // if the history is too short, we try to get more elements from disk
        if (availableFiles > 0 && history.size() < MIN_DEQUE_SIZE && !isRestoring) {
            System.out.println("Restoring from disk");
            isRestoring = true;
            new Thread(this::restoreLastSavedMementosFromDisk).start();
        }
    }

    private void saveToDisk(Collection<SNAPSHOTS> toSave) {
        System.out.println("Saving to disk");

        new Thread(() -> {
            int fileNumber = availableFiles + 1;

            Path savePath = basePath.resolve(String.format(FILENAME, fileNumber));

            List<String> jsonsToSave = new ArrayList<>();
            for (SNAPSHOTS snapshot : toSave) {
                jsonsToSave.add(snapshot.toJson());
            }

            try {
                DeflaterOutputStream dos = new DeflaterOutputStream(Files.newOutputStream(savePath));
                dos.write(objectMapper.writeValueAsBytes(jsonsToSave));
                dos.close();
                availableFiles++;
            } catch (IOException e) {
                isSaving = false;
                e.printStackTrace();
            }
            isSaving = false;
        }).start();
    }

    private int getNumberFromFilename(String filename) {
        return Integer.parseInt(filename.substring("memento_history_".length()));
    }

    private int getNumberOfAvailableFileToRestoreStates() {
        File[] savedFiles = basePath.toFile().listFiles((dir, name) -> name.startsWith("memento_history_"));
        return savedFiles == null ? 0 : savedFiles.length;
    }

    private void restoreLastSavedMementosFromDisk() {
        // List all the files containing "undo" actions
        File[] savedFiles = basePath.toFile().listFiles((dir, name) -> name.startsWith("memento_history_"));
        if (savedFiles == null || savedFiles.length == 0) {
            return;
        }

        // getting the last file that was saved
        File toRestore = Arrays.stream(savedFiles)
                .max((o1, o2) -> getNumberFromFilename(o1.getName()) - getNumberFromFilename(o2.getName()))
                .orElse(null);

        if (toRestore == null) {
            return;
        }

        // restoreStateToInstance the mementos from the file
        try {
            InflaterInputStream iis = new InflaterInputStream(Files.newInputStream(toRestore.toPath()));
            String json = new String(iis.readAllBytes(), StandardCharsets.UTF_8);
            iis.close();

            String[] statesList = objectMapper.readValue(json, String[].class);
            Arrays.asList(statesList)
                    .reversed()
                    .iterator()
                    .forEachRemaining(savedState -> {
                        try {
                            history.addFirst(
                                    concreteSnapshotClass.getDeclaredConstructor(Originator.class, String.class)
                                            .newInstance(originator, savedState)
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });

            Files.delete(toRestore.toPath());
            availableFiles--;

            // set the last saved memento to the last element of the deque
            // inside the variable containing the current display
            if (!history.isEmpty()) {
                lastSavedMemento = history.removeLast();
            }
        } catch (IOException e) {
            e.printStackTrace();
            isRestoring = false;
        }
        isRestoring = false;

    }

    public void forceSaveCurrentHistoryToFile() {
        saveToDisk(history);
    }
}