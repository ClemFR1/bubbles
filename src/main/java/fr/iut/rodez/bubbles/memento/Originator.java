package fr.iut.rodez.bubbles.memento;

public interface Originator<T, U> {

    U createSnapshot();

    void setState(T newState);

}
