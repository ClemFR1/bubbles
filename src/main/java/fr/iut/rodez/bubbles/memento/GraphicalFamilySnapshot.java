package fr.iut.rodez.bubbles.memento;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.service.FamilyService;

public class GraphicalFamilySnapshot implements MementoSnapshot<GraphicalFamily> {

    private final Originator<GraphicalFamily, GraphicalFamilySnapshot> originator;
    private final String jsonState;

    public GraphicalFamilySnapshot(
            Originator<GraphicalFamily, GraphicalFamilySnapshot> o,
            GraphicalFamily state) {
        this.originator = o;

        jsonState = FamilyService.toJson(state);
    }

    public GraphicalFamilySnapshot(
            Originator<GraphicalFamily, GraphicalFamilySnapshot> o,
            String jsonState) {
        this.originator = o;
        this.jsonState = jsonState;
    }

    @Override
    public void restore() {
        originator.setState(getState());
    }

    public GraphicalFamily getState() {
        Family f = FamilyService.fromJson(jsonState);

        if (f == null) {
            throw new IllegalStateException("Could not restore state from JSON");
        }
        return new GraphicalFamily(f);
    }

    @Override
    public String toJson() {
        return jsonState;
    }
}
