package fr.iut.rodez.bubbles.memento;

public interface MementoSnapshot<T> {

    void restore();

    String toJson();

}
