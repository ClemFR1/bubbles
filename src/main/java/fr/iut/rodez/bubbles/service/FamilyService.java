package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.model.GraphicalRelation;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;

public final class FamilyService {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(SNAKE_CASE);
    }

    private FamilyService() {}

    public static FamilyLoadingResult loadFamilyFromFile(File file) {
        try {
            FamilyFile family = objectMapper.readValue(file, FamilyFile.class);
            Set<FamilyMember> members = loadFamilyMembers(family);
            return new FamilyLoadingResult.Loaded(new Family(family.name(), members));
        } catch (Exception e) {
            return new FamilyLoadingResult.LoadingError(e);
        }
    }

    private static Set<FamilyMember> loadFamilyMembers(FamilyFile familyFile) {
        Set<FamilyMember> loadedMembers = familyFile.members()
                                                    .stream()
                                                    .map(FamilyService::toFamilyMember)
                                                    .collect(Collectors.toSet());

        familyFile.relations()
                  .forEach(relation -> createRelation(relation, loadedMembers));

        return loadedMembers;
    }

    private static FamilyMember toFamilyMember(FamilyFile.Member member) {
        FamilyFile.Member.Position fileMemberPosition = member.position();
        Position position = new Position(fileMemberPosition.x(), fileMemberPosition.y());
        Identity identity = new Identity(member.id(), member.name(), member.picture());
        return new FamilyMember(identity, position);
    }

    private static void createRelation(FamilyFile.Relation relation, Set<FamilyMember> members) {
        findById(members, relation.from()).ifPresent(from -> findById(members, relation.to()).ifPresent(to -> {
            switch (relation.type()) {
                case PARENT -> from.parentOf(to);
                case LOVER -> from.inLoveWith(to);
            }
        }));
    }

    private static Optional<FamilyMember> findById(Set<FamilyMember> members, UUID id) {
        return members.stream()
                      .filter(member -> id.equals(member.identity.getId()))
                      .findFirst();
    }

    public sealed interface FamilyLoadingResult {
        record Loaded(Family family) implements FamilyLoadingResult {}

        record LoadingError(Exception cause) implements FamilyLoadingResult {}
    }

    /**
     * Convert a GraphicalFamily to a json string by using a FamilyFile object.
     * @see FamilyService#toFamilyFile(GraphicalFamily)
     * @param family The family to convert
     * @return json string representing the family with the FamilyFile format
     */
    public static String toJson(GraphicalFamily family) {

        // Create a FamilyFile object with the converted objects
        FamilyFile familyFile = toFamilyFile(family);

        // Create the json from the FamilyFile object
        try {
            return objectMapper.writeValueAsString(familyFile);
        } catch (JsonProcessingException ex) {
            System.out.println("Error while converting family to json");
            return null;
        }
    }

    /**
     * Convert a GraphicalFamily to a FamilyFile object by converting all
     * members and relations to the equivalent FamilyFile format
     * @param family The family to convert
     * @return The FamilyFile object
     */
    public static FamilyFile toFamilyFile(GraphicalFamily family) {
        Set<FamilyFile.Relation> relations = new HashSet<>();
        Set<FamilyFile.Member> members = new HashSet<>();
        List<String[]> lovedRelationsSaved = new ArrayList<>();


        // Convert all members and relations to FamilyFile format
        family.relations.forEach(graphicalRelation -> {

                    if (graphicalRelation.target().isLoveWith(graphicalRelation.source())) {
                        for (String[] relation : lovedRelationsSaved) {
                            if (relation[0].equals(graphicalRelation.target().identity().getName()) || relation[0].equals(graphicalRelation.source().identity().getName())
                               && relation[1].equals(graphicalRelation.target().identity().getName()) || relation[1].equals(graphicalRelation.source().identity().getName())) {
                                return;
                            } else {
                                lovedRelationsSaved.add(new String[]{graphicalRelation.target().identity().getName(), graphicalRelation.source().identity().getName()});
                            }
                        }
                    }

                    relations.add(toRelations(graphicalRelation));
                });

        family.members.forEach(graphicalFamilyMember -> members.add(toMember(graphicalFamilyMember)));

        return new FamilyFile(family.name, members, relations);
    }

    /**
     * Convert a GraphicalRelation to a FamilyFile.Relation
     * @param relation The relation to convert
     * @return The converted relation
     */
    private static FamilyFile.Relation toRelations(GraphicalRelation relation) {
        return new FamilyFile.Relation(
                relation.source().identity().getId(),
                relation.target().identity().getId(),
                relation.source().isLoveWith(relation.target()) ? FamilyFile.Relation.Type.LOVER : FamilyFile.Relation.Type.PARENT);
    }

    /**
     * Convert a GraphicalFamilyMember to a FamilyFile.Member
     * @param familyMember The member to convert
     * @return The converted member
     */
    private static FamilyFile.Member toMember(GraphicalFamilyMember familyMember) {
        return new FamilyFile.Member(
                familyMember.identity().getId(),
                familyMember.identity().getName(),
                familyMember.identity().getPicture(),
                new FamilyFile.Member.Position(familyMember.currentPosition().x(), familyMember.currentPosition().y()));
    }

    public static Family fromJson(String json) {
        try {
            FamilyFile familyFile = objectMapper.readValue(json, FamilyFile.class);
            Set<FamilyMember> members = loadFamilyMembers(familyFile);
            return new Family(familyFile.name(), members);
        } catch (Exception e) {
            return null;
        }
    }
}
