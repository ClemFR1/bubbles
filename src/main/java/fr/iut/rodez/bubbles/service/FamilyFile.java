package fr.iut.rodez.bubbles.service;

import java.net.URL;
import java.util.Set;
import java.util.UUID;

public record FamilyFile(
        String name,
        Set<Member> members,
        Set<Relation> relations
) {

    public record Member(
            UUID id,
            String name,
            URL picture,
            Position position
    ) {
        public record Position(
                double x,
                double y
        ) {}
    }

    public record Relation(
            UUID from,
            UUID to,
            Type type
    ) {

        public enum Type {
            PARENT,
            LOVER
        }
    }
}
